# docker run -e TEAMCITY_SERVER=http://buildserver:8111 -dt -p 9090:9090 bvolders/teamcity-sbt-agent

FROM ubuntu:15.10
MAINTAINER Bert Volders <bert.volders@gmail.com>

ADD setup-agent.sh /setup-agent.sh

# Make sure the package repository is up to date.
RUN apt-get update
RUN apt-get -y upgrade

RUN apt-get -y install openjdk-8-jdk wget unzip docker.io nodejs npm sudo
RUN npm install --global gulp bower stylus
RUN adduser --quiet teamcity
# Make sure that the "teamcity" user is part of the "docker" group. Needed to access the docker daemon's unix socket.
RUN usermod -a -G docker teamcity


# some obscure script :o
ADD wrapdocker /usr/local/bin/wrapdocker
RUN chmod +x /usr/local/bin/wrapdocker
VOLUME /var/lib/docker


EXPOSE 9090
CMD sudo -u teamcity -s -- sh -c "TEAMCITY_SERVER=$TEAMCITY_SERVER sh /setup-agent.sh run"